# vnwordembedding

Vietnamese News Word Embedding Pre-training

| File name                | Tool        | Dim    | Case      | Tokenize      | Link        |
| ------------------------ | ----------- | ------:|:---------:|:-------------:| -----------:|
| nc2.low.100.vec          | FastText    | 100    | lower     | No            |[Download](https://gitlab.com/vietthang.hcmus/vnwordembedding/blob/master/nc2.low.100.vec) |
| nc2.low.tok.100.vec      | FastText    | 100    | lower     | Yes           |[Download](https://gitlab.com/vietthang.hcmus/vnwordembedding/blob/master/nc2.low.tok.100.vec) |
| nc2.truecase.100.vec     | FastText    | 100    | truecase  | No            |[Download](https://gitlab.com/vietthang.hcmus/vnwordembedding/blob/master/nc2.truecase.100.vec) |
| nc2.truecase.tok.100.vec | FastText    | 100    | truecase  | Yes           |[Download](https://gitlab.com/vietthang.hcmus/vnwordembedding/blob/master/nc2.truecase.tok.100.vec) |
